Title:Git Workflow 
Date: 2017-02-12
Category: GitLab
Tags: git 
Slug: git-workflow

![datanous logo](./datanous-logo.png)
# Git Workflow

The trick to establishing a version control workflow is striking a solid balance between its benefits and process overhead. A google search on git best practice workflows will yield a wealth of material to wade through, but I found the [Comparing Workflows][6] section of [Atlassian's git tutorial][7] to provide one of the most concise overviews.

I started using version control software on my own projects with the primary objective of being able to develop from and deploy to multiple environments.  Having started with likes of [Subversion][8], I settled into using using Git with a [centralized workflow][1], which provided me with the following key version control system benefits for solo development work.

- Synchronization of work across multiple computers and environments (e.g., development and production)
- Fine-grained change history of work allowing tracking, comparisons, reversions, and controlled deployments
- Backup of source code maintained in a centralized, master repository

However, even while working alone, I found this centralized workflow to be lacking in the following areas.  

- A formal means of maintining separation between production and development work, where the latest, working production code is always clearly identified and available
- A way to maintain development work in a centralized repository so that work in process  is also afforded the same backup and synchronization benefits as production code.

In order to address these gaps, I've moved to a [feature branch][2] type workflow where a master branch is always production-ready and deployable, and development work takes place in a separate feature branch (or branches). A slight increase in discipline is required, but I was always struggling to find informal (and inconsitent) ways to close the gaps anyways. Why not take advantage of the many, many others that have already thought through this and established best practices?

Following are some terse notes on how to apply my current working-alone, feature-based Git workflow using a version control repository management service. This is mostly adopted from [Simple Git Workflow is Simple][3], but also takes into account [Git - When to Merge vs. When to Rebase][4]. There's slightly more overhead than necessary if you are truly working as a sole developer, but with an eye towards working well with others, I think well worth the effort to establish good habits.

## Creating a New Project 

1. Create a new project at [GitLab][5] (or the repository manager of your choice).

    Click on the New Project button and follow the steps.

2. Clone the project repository in your environment and initialize the master branch.

    This forms the master branch, which from this point on should only contain production ready code.

    ```bash
    git clone git@gitlab.com:haafoo/my-project.git
    git clone git@gitlab.com:haafoo/my-project.git
    cd my-project
    touch README.md
    git add README.md
    git commit -m 'Initialize master branch.'
    git push -u origin master
    ```

## Work on a New Feature or Fix

### Intiate a Feature Branch

1. Pull down the latest changes from `master` 

    ```bash
    git checkout master
    git fetch origin
    git merge master
    ```

2. Create a feature branch to isolate your development work

    Using the following `feature/my-feature-name` convention will look nice in some Git gui clients.

    ```bash
    git checkout -b feature/my-feature-name
    ```

### Development Workflow on the Feature Branch

1. Add and commit your work.

    ```bash
    git add .
    git commit -m 'my comment'
    ```

2. Push changes to the central repository, `origin`

    The `-u` argument establishes the remote target, and after the first time you can then `push` without explicitly specifying the target.

    ```bash
    git push -u origin feature/my-feature-name
    ```

3. Periodically synchronize with the latest changes in master.

    This is only required if you (or others) are working on features in parallel.

    ```bash
    git fetch origin master
    git rebase -p master
    ```

    In case you (or others) are committing to the remote feature branch in parallel, you'll also need to `rebase` the changes from there.

    ```bash
    git fetch origin feature/my-feature-name
    git rebase -p feature/my-feature-name
    ```

    **Technical Note**: The `-p` option is the `--preserve-merges` flag, which is required to maintain history in a logical way as explained in [Git - When to Merge vs. When to Rebase][4] (see the section "Hold on, git pull --rebase isn't all gravy!")

    Also, rather than explicitly using `fetch` followed by `rebase`, you can use `pull` with the `--rebase=preserve` option to achieve the same result.

    ```bash
    git pull --rebase=preserve origin master
    ```

    Further, this can be configured to be the default behavior in `.gitconfig` to avoid explicitly typing the `--rebase=preserve` option each time as follows.

    ```bash
    git config --global branch.autosetuprebase always
    git config --global pull.rebase preserve
    ```

    A final note to reconcile the above with some of the statements in [Git - When to Merge vs. When to Rebase][4] post. The `preserve` option to `pull` was made available near the end of 2013 in git 1.8.5.  So, prior to that time, rebasing while preserving merges required an explicit `fetch` and `rebase`.  Even though the referenced blog post is dated February 15, 2016, it was apparently written based on a verion of git prior to 1.8.5. 

4. Resolve merge conflicts by using the `rebase` output message and `git status` to determine where the conflicts occur and edit the files.  Then continue the rebase.

    ```bash
    git add .
    git rebase --continue 
    ```

    Alternatively, abandon the merge

    ```bash
    git rebase --abort 
    ```

## Merge Completed Feature or Fix

1. Merge the feature branch into the master (using and explicit `merge`)

    This approach of merging the final feature branch (while rebasing the feature branch to keep it up to date along the way if necessary) will preserve commit history in a logical way that supports reverting the entire feature.

    ```bash
    git checkout master
    git pull origin master

    git merge --no-ff feature/my-feature-name
    ```

    The merge should not include any changes if the feature branch has been kept up to date with the main branch as described above.  All that remains then is to push the resulting merged master branch to the central repository.

    ```bash
    git push
    ```

    **Note**: In a collaborative workflow, rather than immediately proceding with the merge of the feature branch a pull request would be made through the Git repository management service, for example, GitLab. 

## Undoing Mistakes

This is a _**dangerous operation**_ in that is not reversible as is the case with most other git operations. As emphasized in the [`git reset` section of Atlassian's Git Tutorial][9], you should generally only reset local changes and never ones that have been shared with other developers.

**Final Warning:** This operation _**permanently destroys**_ changes.

Okay, sometimes, especially when working alone, you might want to do this anyways after, for example, discovering you pushed a mistake to the remote master.  You could just fix the mistake and commit again, but for some simple, stupid error that seems messy.  The following is an example of how you can reverse the last commit pushed to the repository.

1. Reset the working directory to match the previous commit.  The `--hard` flag destroys any uncommited changes.

    ```bash
    git reset HEAD~1 --hard
    ```

2. Make the changes you want (without the stupid mistake). Then `commit` and `push` them to re-write history.

    ```bash
    git add .
    git commit -m 'Same message as last time.'
    git push --force
    ```

[1]: https://www.atlassian.com/git/tutorials/comparing-workflows#centralized-workflow
[2]: https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow
[3]: http://blogs.atlassian.com/2014/01/simple-git-workflow-simple
[4]: https://www.derekgourlay.com/blog/git-when-to-merge-vs-when-to-rebase
[5]: https://gitlab.com
[6]: https://www.atlassian.com/git/tutorials/comparing-workflows
[7]: https://www.atlassian.com/git/tutorials
[8]: https://subversion.apache.org/docs
[9]: https://www.atlassian.com/git/tutorials/undoing-changes#git-reset 
