Title: Pelican Theme using CSS Grids
Date: 2017-11-19
Category: GitLab
Tags: python, pelican, website, full stack
Slug: pelican-template

![datanous logo]({filename}/images/datanous-logo.png)
# Pelican Theme using CSS Grids 

Website Goal: Build a website that uses a static generator with the primary purpose of publishing jupyter notebook articles.  Support markdown articles as well, for where jupyter just isn't a good fit, and possibly later support JupyterLab and Zeppelin.


Goal: Build a mobile-first, responsive pelican theme for my datanous website.


 und the [Linked In][1] section of
[Medium][2] to provide one of the most concise overviews.





1. Create a new project at [GitLab][5] (or the repository manager of your choice).

    Click on the New Project button and follow the steps.

2. Clone the project repository in your environment and initialize the master branch.

    This forms the master branch, which from this point on should only contain production ready code.

    ```bash
    git clone git@gitlab.com:haafoo/my-project.git
    git clone git@gitlab.com:haafoo/my-project.git
    cd my-project
    touch README.md
    git add README.md
    git commit -m 'Initialize master branch.'
    git push -u origin master
    ```

## Work on a New Feature or Fix

### Intiate a Feature Branch

1. Pull down the latest changes from `master` 

    ```bash
    git checkout master
    git fetch origin
    git merge master
    ```

2. Create a feature branch to isolate your development work

    Using the following `feature/my-feature-name` convention will look nice in some Git gui clients.

    ```bash
    git checkout -b feature/my-feature-name
    ```

[1]: https://www.smashingmagazine.com/2017/06/building-production-ready-css-grid-layout/
[4]: https://www.smashingmagazine.com/2017/06/building-production-ready-css-grid-layout/
[1]: https://www.linkedin.com/pulse/our-biggest-economic-social-political-issue-two-economies-ray-dalio?articleId=6327163206116667393#comments-6327163206116667393&trk=prof-post 
[2]: https://medium.com/
[3]: https://chrisalbon.com/
